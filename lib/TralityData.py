class TralityData:

    '''
    https://docs.trality.com/trality-code-editor/core-concepts/data/object
    Attributes and underlying data

    The following are all properties of the TralityData class.
    '''

    # symbol information
    symbol
    base
    quoted
    keys


    # time information
    times
    first_time
    last_time

    # price information
    first
    last
    open
    open_last
    high
    high_last
    low
    low_last
    close
    close_last
    price
    price_last
    volume
    volume_last
    oc
    hl
    hlc
    ohlc


    '''
    Standard query and manipulation methods
    '''

    # custom query methods
    def select(key)
    def select_last(key)

    # conversion methods
    def as_pd()
    def as_pandas()
    def as_np()
    def as_numpy()
    def serialize()


    '''
    Utility methods
    '''
    def is_valid()
    def is_root()