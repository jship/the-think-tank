# ON COINBASE PRO
import numpy as np


def make_double_barrier(symbol,amount,take_profit,stop_loss,state):

    """make_double_barrier

    This function creates two iftouched market orders with the onecancelsother
    scope. It is used for our tripple-barrier-method

    Args:
        amount (float): units in base currency to sell
        take_profit (float): take-profit percent
        stop_loss (float): stop-loss percent
        state (state object): the state object of the handler function
    
    Returns:
        TralityOrder:  two order objects
    
    """

    with OrderScope.one_cancels_others():
        order_upper = order_take_profit(symbol,amount,
        								take_profit,
                                        subtract_fees=True)
        order_lower = order_stop_loss(symbol,amount,
        							  stop_loss,
                                      subtract_fees=True)
        
    if order_upper.status != OrderStatus.Pending:
        errmsg = "make_double barrier failed with: {}"
        raise ValueError(errmsg.format(order_upper.error))
    
    # saving orders
    state["order_upper"] = order_upper
    state["order_lower"] = order_lower
    state["created_time"] = order_upper.created_time

    return order_upper, order_lower


def check_max_holding_period(timestamp,state):
    
    """check_max_holding_period

    This function checks the for a first touch in the vertical barrier.
    If the vertical barrier is touched the double barrier orders are canceled.

    Args:
        timestamp (float): milliseconds of current engine time
        state (state object): the state object of the handler function          
    
    Returns:
        bool value

    """

    if check_state_info(state) is None:
        return True
    
    time_delta = timestamp - state["created_time"]
    
    if state["max_period"] is None:
        return True

    # cancel order if vertical barrier reached
    if time_delta >= state["max_period"]:
        print("vertical barrier reached")
        cancel_order(state["order_upper"].id)
        cancel_order(state["order_lower"].id)
        close_position(state["order_lower"].symbol)


    return True


def check_state_info(state):
    
    """check_state_info

    This function checks the state object for relevant order information.
    If the information exists it also refreshes the order_upper from the
    double barrier function

    Args:
        state (state object): the state object of the handler function          
    
    Returns:
        None or TralityOrder order_upper 

    Raises:
        
        AssertionError: If invalid order specification.
    
    """
    
    if "order_upper" not in state.keys():
        return None
    elif state["order_upper"] is None:
        return None
    
    order_upper = state["order_upper"]
    
    errmsg = "No max_period in state. Unable to check max holding period"
    
    assert "max_period" in state.keys() , errmsg
    # refreshing order from api
    order_upper.refresh()
    
    if order_upper.status != OrderStatus.Pending:
                
        # resetting state information
        state["order_upper"] = None
        state["order_lower"] = None
        state["created_time"] = None
        return None

    return order


def last_five_up(data):
    prices = data.select("close")
    # signs = np.sign(np.diff(prices.as_np()))[-5:]
    # TODO: I CANT FIGURE OUT WHY TRALITY WONT LET ME USE NP
    signs = [0, 0 , 0, 0, 0]
    diffs = [0, 0 , 0, 0, 0]
    i = 0
    for x in range(-6, -1):
        diffs[i] = prices[x + 1] - prices[x]
        i += 1
    for n in range(len(diffs)): 
        if diffs[n] > 0:
            signs[n] = 1
        elif diffs[n] < 0:
            signs[n] = -1
    return sum(signs) == 5


def initialize(state):
    state.max_period = None # exclude vertical


@schedule(interval="15min", symbol=["BTC-USD"], window_size=200)
def handler(state, data):
    
    # moving averages on volume
    volume = data.volume
    ema_short_volume, ema_long_volume = volume.ema(20).last, volume.ema(40).last

    # return early on missing data
    if ema_short_volume is None:
        return False 
    
    has_high_volume = ema_short_volume[-1] > ema_long_volume[-1]

    # getting portfolio and position information
    portfolio = query_portfolio()
    buy_value = float(portfolio.excess_liquidity_quoted) * 0.95
    position = query_open_position_by_symbol(data.symbol)
    has_position = position is not None
    
    if not has_position and last_five_up(data) and has_high_volume:
        price = data.close_last
        buy_amount = buy_value / price
        buy_order = order_market_amount(data.symbol,buy_amount)

        # setup barriers
        make_double_barrier(data.symbol,float(buy_order.quantity),
                            0.05,0.03,state)