https://www.trality.com/blog/developing-simple-trading-bot-with-trality-bot-code-editor

	# Define crossovers
def crossed_upwards(short, lng):
    return short[-2] < lng[-2] and short[-1] > lng[-1]

def crossed_downwards(short, lng):
    return short[-2] > lng[-2] and short[-1] < lng[-1]

@schedule(interval="1h", symbol="BTCUSDT")
def handler(state, data):

    # Catch basic indicators
    ema_long = data.ema(40)
    ema_short = data.ema(20)

    # Empty values check, e.g. maintenance windows, etc.
    if any(param is None for param in [ema_long.last, ema_short.last]):
        return

    # Check whether we currently have an open position that's not dust
    has_position = has_open_position(data.symbol, truncated=True)

    # Define EMA conditions
    ema_crossed_upwards = crossed_upwards(ema_short['real'], ema_long['real'])
    ema_crossed_downwards = crossed_downwards(ema_short['real'], ema_long['real'])

    # Compute amount we want to buy (we use 80% of our portfolio here)
    balance_base = float(query_balance_free(data.base))
    balance_quoted = float(query_balance_free(data.quoted))
    buy_amount = balance_quoted * 0.80 / data.close_last

    # Check for buy and sell conditions and allow one open position at a time
    if ema_crossed_upwards and not has_position:
       create_order(symbol=data.symbol,amount=buy_amount)

    elif ema_crossed_downwards and has_position:
       close_position(data.symbol)
