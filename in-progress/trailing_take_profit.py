# The purpose of this file is to wrap the order_trailing_if_touched_amount() function from Trality
# Ideally, this file could be included at the top but I don't know if Trality will allow that
# So for the time, just copy and paste this at the top of a bot's code.
# https://docs.trality.com/trality-code-editor/api-documentation/order/creation/trailing-iftouched

# sell_amount_float: float, The desired amount to sell denoted in BASE CURRENCY, the First part of the pair
def trailing_take_profit(coin_str, sell_amount_float, trailing_percent_float, sell_price):
    sell_amount = sell_amount_float
    if sell_amount_float > 0:
        sell_amount *= -1
        print("FLIPPED A TRAILING TAKE PROFIT AMOUNT")
    return order_trailing_iftouched_amount(coin_str, sell_amount_float, trailing_percent_float, sell_price)


# def trailing_stop_loss(coin_str, sell_amount_float, trailing_percent_float, sell_price):

# May not even need to test it if we can trust Trality,
# could just debug to make sure the wrapper functions works.
# Maybe for ease, make the symbol USD-ETH?
def test_ttp():
    # Test Case 1:
    # Set up: Have bought at least 1 ETH.
    # Case 1: Sell all 1 ETH at 1000 dollars. In the case that the price goes above 1000, then 
    # Case 2: Sell all 1 ETH at 1100 dollars.

    sell_amount = 1.0 # ETH
    symbol = "ETH-USD"
    trailing_percent = 0.1
    sell_price = 1000
    order = trailing_take_profit("ETH-USD", 1.0, 0.1, sell_price)
# print("-- run {}".format(state.run))
# print("closing price {}".format(last_closing_price))








# 17.03.20 to 20.03.20 original test dates at 1hr timeframe

def trailing_take_profit(coin_str, sell_amount_float, trailing_percent_float, sell_price):
    sell_amount = sell_amount_float
    if sell_amount_float > 0:
        sell_amount *= -1
        print("FLIPPED A TRAILING TAKE PROFIT AMOUNT")
    return order_trailing_iftouched_amount(coin_str, sell_amount_float, trailing_percent_float, sell_price)

def initialize(state):
    state.number_offset_trades = 0;


@schedule(interval="1h", symbol="BTC-USD")
def handler(state, data):
    '''
    1) Compute indicators from data
    '''
    
    ema_long = data.ema(40).last
    ema_short = data.ema(20).last
    rsi = data.rsi(14).last
    
    # on erroneous data return early (indicators are of NoneType)
    if rsi is None or ema_long is None:
        return

    current_price = data.close_last

    
    portfolio = query_portfolio()
    balance_quoted = portfolio.excess_liquidity_quoted
    # we invest only 80% of available liquidity
    buy_value = float(balance_quoted)
    

    position = query_open_position_by_symbol(data.symbol,include_dust=False)
    has_position = position is not None
    bought_once = False
    if (ema_short > ema_long and rsi < 40) and not bought_once:
        print("-------")
        print("Buy Signal: creating market order for {}".format(data.symbol))
        print("Buy value: ", buy_value, " at current market price: ", data.close_last)
        
        order_market_value(symbol=data.symbol, value=buy_value)
        bought_once = True

    if bought_once:
        # NOTE TO SELF. For some reason, this works. Above, it buys 0.19600, it tries to sell at 0.19600. That fails
        # what exists below, is that it buys at the same, then sells at 0.001. That works. 
        trailing_percent_float = 0.08
        buy_value = buy_value * 0.98
        sell_price = buy_value# * 1.1
        order_trailing_iftouched_value(data.symbol, -buy_value, trailing_percent_float, sell_price)
        bought_once = False

    # elif not has_position:
        # print("-------")
        # logmsg = "Sell Signal: closing {} position with exposure {} at current market price {}"
        # print(logmsg.format(data.symbol,float(position.exposure),data.close_last))

        # close_position(data.symbol)
       
    '''
    5) Check strategy profitability
        > print information profitability on every offsetting trade
    '''
    
    if state.number_offset_trades < portfolio.number_of_offsetting_trades:
        
        pnl = query_portfolio_pnl()
        print("-------")
        print("Accumulated Pnl of Strategy: {}".format(pnl))
        
        offset_trades = portfolio.number_of_offsetting_trades
        number_winners = portfolio.number_of_winning_trades
        print("Number of winning trades {}/{}.".format(number_winners,offset_trades))
        print("Best trade Return : {:.2%}".format(portfolio.best_trade_return))
        print("Worst trade Return : {:.2%}".format(portfolio.worst_trade_return))
        print("Average Profit per Winning Trade : {:.2f}".format(portfolio.average_profit_per_winning_trade))
        print("Average Loss per Losing Trade : {:.2f}".format(portfolio.average_loss_per_losing_trade))
        # reset number offset trades
        state.number_offset_trades = portfolio.number_of_offsetting_trades